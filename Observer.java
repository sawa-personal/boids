//*******************************************************
// Observer.java
// created by Sawada Tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータから座標情報を観測し，Processingに渡すためのクラス */

import java.util.*;

public class Observer {
    public static final int num = Space.network.num; //粒子の総数
    private static Space space; //粒子が動き回る空間のインスタンス

    //粒子が存在する空間を生成し，その初期座標を返す
    public static Vector[][] initPosition(final int WIDTH, final int HEIGHT) {
	space = new Space();
	return getPosition(WIDTH, HEIGHT);
    }

    //系を動かす
    public static void run() {
	space.run();
    }
    
    //粒子の座標を返す
    public static Vector[][] getPosition(final double WIDTH, final double HEIGHT) {
	Vector position[] = new Vector[num]; //座標格納用インスタンスの生成
	Vector velocity[] = new Vector[num]; //速度格納用インスタンスの生成
	Vector[][] trianglePosition = new Vector[num][3];
	for(int i = 0; i < num; i++){
	    position[i] = new Vector(); //座標のインスタンス
	    velocity[i] = new Vector(); //座標のインスタンス
	    Agent agent = space.network.agent[i]; //着目エージェント
	    position[i].set(agent.p.getx(), agent.p.gety()); //空間内の粒子から座標情報を取得
	    velocity[i].set(agent.v.getx(), agent.v.gety()); //空間内の粒子から座標情報を取得
	    Vector u = Vector.normalize(velocity[i], 20); //速度を正規化
	    int deg = 60;
	    double x0 = position[i].getx();
	    double y0 = position[i].gety();
	    double x1 = x0 + u.getx() * Math.cos(deg) - u.gety() * Math.sin(deg);
	    double y1 = y0 + u.getx() * Math.sin(deg) + u.gety() * Math.cos(deg);
	    double x2 = x0 + u.getx() * Math.cos(deg) + u.gety() * Math.sin(deg);
	    double y2 = y0 - u.getx() * Math.sin(deg) + u.gety() * Math.cos(deg);
	    trianglePosition[i][0] = new Vector(x0, y0); //頂点の座標
	    trianglePosition[i][1] = new Vector(x1, y1); //左下の点の座標
	    trianglePosition[i][2] = new Vector(x2, y2); //右下の点の座標
	}
	return trianglePosition;
    }

    public static float[][] getColor() {
	float color[][] = new float[num][3];
	for(int i = 0; i < num; i++){
	    Agent agent = space.network.agent[i]; //着目エージェント
	    color[i][0] = 255 * (float)agent.sepColor; //分離度:赤
	    color[i][1] = 255 * (float)agent.algColor; //整列度:緑
	    color[i][2] = 255 * (float)agent.cohColor; //結合度:青
	}
	return color;
    }
}
