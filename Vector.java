//*******************************************************
// Vector.java
// created by Sawada Tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ベクトルのオブジェクト */

import java.util.*;

public class Vector {
    protected double x[]; // 各成分値
    protected int size; //次元数
    
    public Vector(double... x){
	size = x.length;
	this.x = new double[size];
	for(int i = 0; i < size; i++){
	    this.x[i] = x[i];
	}
    }
    
    public void set(double... x){
	size = x.length;
	this.x = new double[size];
	for(int i = 0; i < size; i++){
	    this.x[i] = x[i];
	}
    }
    
    public void setx(double x) {
	this.x[0] = x;
    }
    
    public void sety(double y) {
	this.x[1] = y;
    }
    
    public double getx(){
	return this.x[0];
    }
    
    public double gety(){
	return this.x[1];
    }
    
    public void add(double x, double y){
	this.x[0] += x;
	this.x[1] += y;
    }

    /******************** start: ベクトル計算系 ********************/
    //ベクトルa,b間の距離を返す
    public static double distance(Vector a, Vector b){
	int aDim = a.size; //ベクトルaの次数
	int bDim = b.size; //ベクトルbの次数
	//aとbの次数が異なれば異常終了
	if(aDim != bDim) {
	    System.exit(1);
	}
	double d = 0; //ab間距離の二乗
	for(int i = 0; i < aDim; i++) {
	    d += Math.pow(a.x[i] - b.x[i], 2); //ab間の各成分の差を二乗して加算
	}
	return Math.sqrt(d); //ルートを取って距離を返す
    }
    
    //ベクトルa,bの加算
    public static Vector addVector(Vector a, Vector b){
	int aDim = a.size; //ベクトルaの次数
	int bDim = b.size; //ベクトルbの次数
	//aとbの次数が異なれば異常終了
	if(aDim != bDim) {
	    System.exit(1);
	}
	
	double component[] = new double[aDim]; //各成分に対応する配列を用意
	for(int i = 0; i < aDim; i++){
	    component[i] = a.x[i] + b.x[i];
	}
	
	Vector x = new Vector(component[0], component[1]);
	return x; //和ベクトルを返す
    }

    //a→bベクトルを返す
    public static Vector subtract(Vector a, Vector b){
	int aDim = a.size; //ベクトルaの次数
	int bDim = b.size; //ベクトルbの次数
	//aとbの次数が異なれば異常終了
	if(aDim != bDim) {
	    System.exit(1);
	}
	Vector x = new Vector(b.x[0] - a.x[0], b.x[1] - a.x[1]); //a→bのベクトル
	return x;
    }
    
    //ベクトルを正規化する
    public static Vector normalize(Vector a, double w){
	Vector zero = new Vector(0, 0);
	double len = distance(a, zero); //aの長さ
	if(len != 0) {
	    return new Vector(w * a.x[0] / len, w * a.x[1] / len);
	}
	return zero;
    }
    /******************** end: ベクトル計算系 ********************/
}
