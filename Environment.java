//*******************************************************
// Environment.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 環境のオブジェクト．何かしらの評価関数を与えることもできる． */

import java.util.*;

public class Environment {
    public static final int DIM = 2; //空間次元
    public static final int WIDTH = 800; //空間の横幅
    public static final int HEIGHT = 600; //空間の縦幅

    //環境を動かす
    public static void run(){
	;
    }
}
