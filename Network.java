//*******************************************************
// Network.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ネットワークのクラス．ノード(Agent)とエッジ(Relation)を要素に持つ */

import java.util.*;

public class Network {
    public Agent agent[]; //ネットワークのノード
    public Relation relation[][]; //ネットワークのエッジ
    private double distance[][]; //ネットワークのエッジ
    public Space space; //このネットワークを持つ空間

    public static int num = 200; //ネットワークのノード数
    private static Random rnd = new Random(); //乱数生成用インスタンス
    
    public Network(Space space) {
	this.space = space; //このネットワークを持つ空間を設定
	/* start: ノードの初期設定 */
	agent = new Agent[num];
	for(int i = 0; i < num; i++) {
	    agent[i] = new Agent(i, this); //ノードのインスタンス生成
	    agent[i].id = i; //IDを設定
	    agent[i].setPosition(rnd.nextInt(space.environment.WIDTH), rnd.nextInt(space.environment.HEIGHT)); //ランダムな座標
	    double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
	    agent[i].setVelocity(3 * Math.random() * Math.cos(r), 3 * Math.random() * Math.sin(r)); //大きさ一定，方向ランダムな速度
	}
	/* end: ノードの初期設定 */
	
	/* start: エッジの初期設定 */
	distance = new double[num][num];
	//relation = new Relation(this); //エッジのインスタンス生成
	for(int i = 0; i < num; i++) {
	    for(int j = i + 1; j < num; j++) {
		distance[i][j] = Vector.distance(agent[i].p, agent[j].p);
		distance[j][i] = distance[i][j];
	    }
	}
	/* end: エッジの初期設定 */
    }

    public void run(){
	for(int i = 0; i < num; i++) {
	    agent[i].move(); //ノードを規則に従って同時に動かす
	}
	//距離情報の更新
	for(int i = 0; i < num; i++) {
	    for(int j = i + 1; j < num; j++) {
		distance[i][j] = Vector.distance(agent[i].p, agent[j].p);
		distance[j][i] = distance[i][j];
	    }
	}
    }

    //ノードidから他ノードまでの距離を返す
    public double[] getDistance(int id){
	double idDistance[] = new double[num];
	for(int i = 0; i < num; i++){
	    idDistance[i] = distance[id][i];
	}
	return idDistance;
    }    
}
