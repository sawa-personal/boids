//*******************************************************
// Agent.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントのオブジェクト */

import java.util.*;

public class Agent {
    public Vector p; //座標
    public Vector v; //速度
    private double[] distance; //距離情報格納用
    private final double CLOSE = 20; // 「近い」距離
    private final double FAR = 80; // 「遠い」距離

    private double range = 100; //認識範囲
    
    private final double MAXVELOCITY = 4.0; //粒子の最大速度
    public int id; //エージェントのID
    private Network network; //属するネットワーク
    public float sepColor; //分離戦略の度合い
    public float cohColor; //結合戦略の度合い
    public float algColor; //整列戦略の度合い
    
    public Agent(int id, Network network) {
	p = new Vector(); //座標
	v = new Vector(); //速度
	this.id = id; //IDを設定
	this.network = network; //属するネットワークを設定
	distance = new double[network.num]; //距離情報格納配列を用意
    }    

    //座標を設定
    public void setPosition(double... x) {
	p.set(x);
    }
    
    //速度を設定
    public void setVelocity(double... x) {
	v.set(x);
    }

    public double getx() {
	return p.x[0];
    }
    
    public double gety() {
	return p.x[1];
    }

    //エージェントを動かす
    public void move() {
	measure(); //他エージェントとの距離を測定
	
	Vector aSep = new Vector(0, 0); //逃げる加速度
	Vector aCoh = new Vector(0, 0); //近づく加速度
	Vector aAlg = new Vector(0, 0); //移動方向を合わせる加速度
	int sepCount = 0; //近くにいたエージェントの数
	int cohCount = 0; //遠くにいたエージェントの数
	int algCount = 0; //ほどよい距離にいたエージェントの数
	for(int i = 0; i < network.num; i++) {
	    if(i == id || distance[i] > range) { continue; } //自分以外の近傍エージェントと比較する
	    
	    Vector u; //ベクトル
	    if(distance[i] < CLOSE) { //近くに居るとき，分離
		u = Vector.subtract(network.agent[i].p, this.p); //相手から自分への方向ベクトル
		u = Vector.normalize(u, 1.0); //正規化
		aSep.add(u.getx(), u.gety());
		sepCount++;
	    } else if(distance[i] > FAR) { //遠くに居るとき，結合
		u = Vector.subtract(this.p, network.agent[i].p); //自分から相手への方向ベクトル
		u = Vector.normalize(u, 1.0); //正規化
		aCoh.add(u.getx(), u.gety());
		cohCount++;
	    } else { //ほどほどのとき，整列
		u = network.agent[i].v; //周りのエージェントの速度をコピー
		u = Vector.normalize(u, 1.0); //正規化
		aAlg.add(u.getx(), u.gety());
		algCount++;
	    }
	}
	if(sepCount != 0){
	    Vector a = new Vector(aSep.getx() / sepCount, aSep.gety() / sepCount);
	    this.v.add(a.getx(), a.gety());
	}
	if(cohCount != 0){
	    Vector a = new Vector(aCoh.getx() / cohCount, aCoh.gety() / cohCount);
	    this.v.add(a.getx(), a.gety());
	}
	if(algCount != 0){
	    Vector a = new Vector(aAlg.getx() / algCount, aAlg.gety() / algCount);
	    this.v.add(a.getx(), a.gety());
	}
	v = Vector.normalize(v, MAXVELOCITY); //最大速度で正規化
	p.add(v.x[0], v.x[1]); //vの方向に進む
	regulate(); //エージェントが環境の領域外に出たときの補正
	setColor(sepCount, cohCount, algCount); //とった戦略の度合いを設定
    }
    
    //他エージェント間との距離を測定
    private void measure() {
	distance = network.getDistance(id); //ネットワークから距離情報を取得
    }
    
    //エージェントが環境の領域外に出たとき跳ね返す
    private void regulate(){
	//walled(); //壁で跳ね返す
	torus(); //2次元トーラス
    }
    private void walled(){
	if(p.getx() > network.space.environment.WIDTH) {
	    p.setx(network.space.environment.WIDTH); //位置を領域の端に
	    v.setx(-v.getx()); //速度を逆方向に
	} else if(p.getx() < 0) {
	    p.setx(0); //位置を領域の端に
	    v.setx(-v.getx()); //速度を逆方向に
	}
	if(p.gety() > network.space.environment.HEIGHT) {
	    p.sety(network.space.environment.HEIGHT); //位置を領域の端に
	    v.sety(-v.gety()); //速度を逆方向に
	} else if(p.gety() < 0) {
	    p.sety(0); //位置を領域の端に
	    v.sety(-v.gety()); //速度を逆方向に	    
	}    
    }

    private void torus(){
	double agentSize = 20; //エージェントの大きさ
	if(p.getx() > network.space.environment.WIDTH + agentSize) {
	    p.setx(-agentSize); //位置を領域の端に
	} else if(p.getx() < -agentSize) {
	    p.setx(network.space.environment.WIDTH + agentSize); //位置を領域の端に
	}
	if(p.gety() > network.space.environment.HEIGHT + agentSize) {
	    p.sety(-agentSize); //位置を領域の端に
	} else if(p.gety() < -agentSize) {
	    p.sety(network.space.environment.HEIGHT + agentSize); //位置を領域の端に
	}    
    }
    
    private void setColor(int sepCount, int cohCount, int algCount) {
	float sum = sepCount + cohCount + algCount;
	//孤立エージェントを白にする
	if(sum == 0) {
	    sepColor = 1;
	    cohColor = 1;
	    algColor = 1;	    
	} else {
	    sepColor = sepCount / sum;
	    cohColor = cohCount / sum;
	    algColor = algCount / sum;
	}
    }
}
